var irc         = require('irc');
var RssFetcher  = require('./fetch');

//define the logger and its settings
var Krokologger = require('./krokolog');
var logger      = new Krokologger({isLog: true});

var ircParams   = require('./irc_params');

var client = new irc.Client(ircParams.serverName, ircParams.selfNick, ircParams.connectOptions);

var krokoBot = {
    rssMessages:            [],
    rssLastUpdated:         new Date(),
    timeoutPeriodSeconds:   1000 * 60 * 30,  //say something each 30 seconds
    maxDiffBeforeRssUpdate: 60 * 60 * 12
};

krokoBot.doneFetch = function(currentFetcher, result) {
    var _self = this;

    _self.rssMessages       = _self.rssMessages.concat(result);
    _self.rssLastUpdated    = new Date();
};

/**
 * Add different fetchers for different sources.
 * TBD: refactor this... may be in future :)
 *
 * @client irc object
 */
krokoBot.setFetchers = function() {
    var _self = this;

    var fetchers = ircParams.rssFetchers;
    var i,
        currentFetcher;

    for(i in fetchers) {
        currentFetcher = fetchers[i];

        currentFetcher.fetcher =
            new RssFetcher(currentFetcher.url,
                        function(result) { _self.doneFetch(currentFetcher, result); },
                        currentFetcher.dataType);

        currentFetcher.fetcher.fetch();
    }
};
krokoBot.setFetchers();

krokoBot.randMsg = function() {
    var _self = this;

    var length = _self.rssMessages.length;
    var rnd;

    var now     = new Date();
    var diff    = (now - _self.rssLastUpdated) / 1000; //in seconds

    if ( length === 0 || (diff > _self.maxDiffBeforeRssUpdate) ) {
        //empty all messages
        _self.rssMessages = [];

        //and fetch new messages
        _self.setFetchers();

        return false;
    } else {
        rnd = Math.floor(Math.random() * length);
    }

    return _self.rssMessages.splice(rnd, 1);
};

krokoBot.actions = {
    '!randMsg':         krokoBot.randMsg,

    '!addressAnswer':   function() {}
};

//------------------------ End parameters block -------------------------


client.addListener('error', function(err) {
    logger.log('--- Error: ' + (new Date()) );
    logger.log(err);
});


/**
 * set timeout for saying some phrases in period
 */
krokoBot.fetchPeriodically = function fetchPeriodically() {
    var _self = this;

    if ( client.maxLineLength ) {
        var j = Math.floor(Math.random() * ircParams.channels.length);

        _self.processAction({
            action:     '!randMsg',
            receiver:   ircParams.channels[j]
        });
    }

    setTimeout(_self.fetchPeriodically.bind(_self), _self.timeoutPeriodSeconds);
}


krokoBot.registerCallBack = function(msg) {
    var _self = this;

    //var endConnect = new Date();
    //logger.log('connected: ' + new Date());
    //logger.log('Time to connect (seconds): ' + (endConnect - beginConnect) / 1000);
    //logger.log('registered msg: ');
    //logger.log(msg);
    //logger.log('-------------------------------------------');

    setTimeout(_self.fetchPeriodically.bind(_self), _self.timeoutPeriodSeconds);
};
client.on('registered', function(msg) { krokoBot.registerCallBack(msg); });


krokoBot.motdCallBack = function(msg) {
    //logger.log(msg);
};
client.on('motd', function(msg) { krokoBot.motdCallBack(msg); });


krokoBot.messageCallBack = function(from, to, msg, msgObj) {
    var _self = this;

    var receiver = (to.indexOf('#') > -1) ? to : from ;

    if ( msg.indexOf(ircParams.selfNick) > -1 ) {
        if ( _self.previousFrom === from ) {
            _self.previousQtyFrom++;
        } else {
            _self.previousFrom = from;
            _self.previousQtyFrom = 1;
        }

        if ( _self.previousQtyFrom <= 3 ) {
            _self.processAction({
                action:     '!addressAnswer',
                receiver:   receiver,
                from:       from
            });
        }

    }
};
client.on('message',function(from,to,msg,msgObj){krokoBot.messageCallBack(from,to,msg,msgObj);});

/**
 * Say something depending on action.
 *
 * @param action
 * @receiver string
 */
krokoBot.processAction = function processAction(params)
{
    var _self = this;

    var action      = params.action;
    var receiver    = params.receiver;

    if ( _self.actions[action] ) {
        switch (action) {
            case    '!randMsg':
                var message = _self.actions['!randMsg'].call(_self);
                if ( message ) {
                    client.say(receiver, message);
                }

                break;
            case    '!addressAnswer':
                var rnd = Math.floor(Math.random() * (ircParams.adressAnswers.length));
                var txt = params.from + " " + ircParams.adressAnswers[rnd];
                client.say(receiver, txt);

                break;
            default:
                logger.log('Unknown action: ' + action);
                break;
        }
    } else {
        logger.log('Unknown action: ' + action);
    }
}



client.on('notice', function(from, to, text, msg) {});

//var beginConnect = new Date();
//logger.log('begin connect: ' + beginConnect);

client.connect();


