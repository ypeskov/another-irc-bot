var request     = require('request')
  , FeedParser  = require('feedparser')
  , Iconv       = require('iconv').Iconv;

var Krokologger = require('./krokolog');
var logger = new Krokologger({isLog: true});

function RssFetcher(feed, doneFetch, dataType)
{
    this.feed                   = feed;

    if ( dataType ) {
        this.dataType           = dataType;
    } else {
        this.dataType           = 'description';
    }

    this.posts          = [''];
    this.doneFetch      = doneFetch;
}

/**
 *
 * @param string receiver where to say message
 */
RssFetcher.prototype.fetch = function(dataType) {
    var _self = this;

    if ( dataType ) {
        _self.dataType = dataType;
    }

    _self.requestSrc();
};

RssFetcher.prototype.requestSrc = function() {
    var _self = this;

    // Define our streams
    logger.log('--- Requesting RSS source: ' + _self.feed);

    //clear the previous values of posts;
    _self.posts = [];

    var req = request(_self.feed, {timeout: 10000, pool: false});
    req.setMaxListeners(20);

    // Some feeds do not respond without user-agent and accept headers.
    req.setHeader('user-agent',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36');
    req.setHeader('accept', 'text/html,application/xhtml+xml');

    var feedparser = new FeedParser();

    // Define our handlers
    req.on('error', function(err) { _self.done(err); });

    req.on('response', function(res) {
        if (res.statusCode != 200) return this.emit('error', new Error('Bad status code'));

        var charset = _self.getParams(res.headers['content-type'] || '').charset;

        res = _self.maybeTranslate(res, charset);

        // And boom goes the dynamite
        res.pipe(feedparser);
    });

    feedparser.on('error', function(err) { logger.log(err, err.stack); });

    feedparser.on('end', function(err) {
        if ( err ) {
            logger.log(err, err.stack);
        }

        _self.doneFetch(_self.posts);
    });

    feedparser.on('readable', function() {
        var post;

        while (post = this.read()) {
            _self
                .posts
                .push(
                    post[_self.dataType]
                        .replace(/<br>/g, '\n')
                        .replace(/&quot;/g, '"')
                );
        }
    });
};

RssFetcher.prototype.maybeTranslate = function(res, charset) {
    var _self = this;
  var iconv;
  
  // Use iconv if its not utf8 already.
  if (!iconv && charset && !/utf-*8/i.test(charset)) {
    try {
      iconv = new Iconv(charset, 'utf-8');

      iconv.on('error', function(err) { logger.log('error in mayBeTranslate')  });
      
      // If we're using iconv, stream will be the output of iconv
      // otherwise it will remain the output of request
      res = res.pipe(iconv);
    } catch(err) {
      res.emit('error', err);
    }
  }
  
  return res;
};

RssFetcher.prototype.getParams = function(str) {
  var params = str.split(';').reduce(function (params, param) {
    var parts = param.split('=').map(function (part) { return part.trim(); });
    if (parts.length === 2) {
      params[parts[0]] = parts[1];
    }
    return params;
  }, {});
  return params;
};


module.exports = RssFetcher;
