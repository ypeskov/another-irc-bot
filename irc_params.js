//define the logger and its settings
var Krokologger = require('./krokolog');
var logger      = new Krokologger({isLog: true});

//------------------- End require block ------------------------


var ircParams = {};

ircParams.channels    = [
    '#autoua'
    //'#vagrant'
    //'#zio'
];
ircParams.serverName  = 'irc.odessa.ua';
ircParams.selfNick    = 'kroko-bot';

ircParams.connectOptions = {
    userName:   'botik',
    realName: 'Krokodil bot',
    port: 7001,
    localAddress: null,
    debug: false,
    showErrors: false,
    autoRejoin: false,
    autoConnect: false,
    channels: ircParams.channels,
    secure: false,
    selfSigned: false,
    certExpired: false,
    floodProtection: false,
    floodProtectionDelay: 1000,
    sasl: false,
    stripColors: true,
    channelPrefixes: "&#",
    messageSplit: 512,
    encoding: 'utf8'
};


//RSS fetch object
ircParams.rssFetchers = {
    "bash.org": {
        "url":      "http://bash.im/rss/",
        "dataType": 'description'
    },
    "anekdot.ru": {
        "url":      "http://www.anekdot.ru/rss/export_top.xml",
        "dataType": 'description'
    },
    "censor.net.ua": {
        "url":  "http://censor.net.ua/includes/resonance_ru.xml",
        "dataType": 'description'
    }
};

ircParams.adressAnswers = [
    'ахз',
    'никогда так не делай больше',
    'тыкать будешь пальцем в подоконник',
    'спроси у пацанов на раЁне',
    'никогда об этом не думал',
    'это просто какой-то позор',
    'злой ты зайчонок, недобрый',
    'Ой, давай только без этого: "Меня трудно найти, но легко потерять". Ты чё, носок в натуре?!',
    'Хорово',
    'А вы не торопитесь?',
    'Как у колобка — слева и справа одинаково',
    'В Анголе дети голодают, а так все в порядке',
    'Задай другой вопрос пожалуйста',
    'Завидуйте молча'
];


module.exports = ircParams;