function KrokoLogger(params)
{
    if ( params === undefined ) {
        params = {};
    }

    this.isLog = ('isLog' in params) ? params.isLog  === true : false ;
}

KrokoLogger.prototype.log = function(msg) {
    var _self = this;

    if ( _self.isLog ) {
        console.log(msg);
    }
};

module.exports = KrokoLogger;